#! /bin/sh

if [ -n "$GUIX_ENVIRONMENT" ]
then
    export GCC_PATH_PREFIX=$GUIX_ENVIRONMENT/bin/arm-linux-gnueabihf-
else
    export GCC_PATH_PREFIX=/opt/gcc-linaro-arm-linux-gnueabihf-4.9-2014.09_linux/bin/arm-linux-gnueabihf-
    export PATH=/home/$USER/git_repos/u-boot-xlnx/tools/:$PATH
fi
export PATH=$(pwd)/bin:$PATH
export L4ARCH=arm
export ARCH=arm
export CROSS_COMPILE=arm-linux-
